<xsl:stylesheet version="2.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:dat="http://fault-ggd-merge-dataflow1/dataservice">
<xsl:output method="text" encoding="iso-8859-1"/>

<xsl:strip-space elements="*" />

<xsl:template match="/dat:Entries/jsonObject/error">
<xsl:for-each select="child::*">
<xsl:if test="position() != last()">
				<xsl:value-of select="name()" />,</xsl:if>
			<xsl:if test="position()= last()">
				<xsl:value-of select="name()" />
				<xsl:text>&#10;</xsl:text>
				<xsl:text> </xsl:text>
			</xsl:if>
			</xsl:for-each>
			

<xsl:for-each select="child::*">
<xsl:if test="position() != last()">
				<xsl:value-of select="normalize-space(.)" />,</xsl:if>
			<xsl:if test="position()= last()">
				<xsl:value-of select="normalize-space(.)" />
				<xsl:text>&#10;</xsl:text>
				<xsl:text> </xsl:text>
			</xsl:if>
</xsl:for-each>


</xsl:template>

</xsl:stylesheet>